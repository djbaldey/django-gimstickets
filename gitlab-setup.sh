#!/bin/bash

# Данный скрипт предназначен для развёртывания проекта в среде CI GitLab и
# дальнейшего автоматического тестирования командами в ".gitlab-ci.yml".

log_info() { echo -e "\e[32m${1}\e[0m"; };

log_info "First update system packages.";
apt-get update -qy;
apt-get install -y git;
# apt-get install -y sudo;

# log_info "Configure the PostgreSQL server for current user 'root'.";
# apt-get install -y postgresql;
# service postgresql start;
# service postgresql status;
# sudo -u postgres psql -d template1 -c "CREATE USER root WITH PASSWORD 'root' SUPERUSER;";
# Проверка доступа от root.
# psql -d template1 -c "SELECT now();";

log_info "Install the python packages to virtual environment.";
apt-get install -y virtualenv python3-dev python3-virtualenv python3-pip;
# apt-get install -y python3-psycopg2;
virtualenv -p python3 --system-site-packages .env;
source ./.env/bin/activate;
pip3 install -r requirements.txt;
pip3 install -e .;

log_info "Setup id done";
exit 0
