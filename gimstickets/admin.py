#
# Copyright (c) 2021, Grigoriy Kramarenko <root@rosix.ru>
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
from django.contrib import admin
from gimstickets.models import Kit, Question, Answer

register = admin.site.register

register(Kit)
register(Question)
register(Answer)
