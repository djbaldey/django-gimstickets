#
# Copyright (c) 2021, Grigoriy Kramarenko <root@rosix.ru>
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
import logging
from asgiref.sync import sync_to_async

from django.shortcuts import render, get_object_or_404

from gimstickets.models import Kit, Question, Answer

logger = logging.getLogger(__name__)


@sync_to_async
def index(request):
    kits = Kit.objects.filter(is_active=True)
    ctx = {'kits': kits}
    return render(request, 'gimstickets/index.html', ctx)


@sync_to_async
def tickets(request, kit_id):
    kit = get_object_or_404(Kit.objects.filter(is_active=True), id=kit_id)
    qs = Question.objects.filter(kit=kit)
    tickets = qs.order_by('ticket').values_list('ticket', flat=True).distinct()
    ctx = {
        'kit': kit,
        'tickets': tickets,
    }
    return render(request, 'gimstickets/tickets.html', ctx)


@sync_to_async
def ticket(request, kit_id, ticket):
    kit = get_object_or_404(Kit.objects.filter(is_active=True), id=kit_id)
    qs = Answer.objects.filter(question__kit=kit, question__ticket=ticket)
    qs = qs.select_related('question')
    qs = qs.order_by('question__number', 'id')
    ctx = {
        'kit': kit,
        'ticket': ticket,
        'answers': qs,
    }
    qs = Question.objects.filter(kit=kit)
    tickets = qs.order_by('ticket').values_list('ticket', flat=True).distinct()
    if list(tickets)[-1] > ticket:
        ctx['next_ticket'] = ticket + 1
    return render(request, 'gimstickets/ticket.html', ctx)
