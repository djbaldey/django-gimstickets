#
# Copyright (c) 2021, Grigoriy Kramarenko <root@rosix.ru>
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
from django.urls import path
from gimstickets.views import index, tickets, ticket

app_name = 'gimstickets'

urlpatterns = [
    path('', index, name='index'),
    path('<int:kit_id>/tickets/', tickets, name='tickets'),
    path('<int:kit_id>/tickets/<int:ticket>/', ticket, name='ticket'),
]
