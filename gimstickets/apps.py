#
# Copyright (c) 2021, Grigoriy Kramarenko <root@rosix.ru>
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
from django.apps import AppConfig


class GimsTicketsConfig(AppConfig):
    default_auto_field = 'django.db.models.AutoField'
    name = 'gimstickets'
