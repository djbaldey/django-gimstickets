#
# Copyright (c) 2021, Grigoriy Kramarenko <root@rosix.ru>
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
from django.db import models


class Kit(models.Model):
    """
    Модель комплекта экзаменационных билетов.
    """
    id = models.IntegerField(primary_key=True)
    description = models.CharField('описание', max_length=512)
    questions = models.PositiveIntegerField('кол-во вопросов')
    answers = models.PositiveIntegerField('минимум правильных')
    minutes = models.PositiveIntegerField('кол-во минут')
    is_active = models.BooleanField('активен', default=True, db_index=True)

    class Meta:
        ordering = ('id',)
        verbose_name = 'комплект'
        verbose_name_plural = 'комплекты'

    def __str__(self):
        return 'Комплект %d' % self.id


class Question(models.Model):
    """
    Модель вопроса в билете.
    """
    id = models.IntegerField(primary_key=True)
    kit = models.ForeignKey(
        Kit, on_delete=models.CASCADE, verbose_name='комплект')
    ticket = models.PositiveIntegerField('билет', db_index=True)
    number = models.PositiveIntegerField('номер вопроса', db_index=True)
    text = models.CharField('текст вопроса', max_length=512)

    class Meta:
        ordering = ('kit', 'ticket', 'number')
        verbose_name = 'вопрос'
        verbose_name_plural = 'вопросы'

    def __str__(self):
        return self.text

    @property
    def picture(self):
        """Возвращает название файла картинки для этого вороса."""
        return '%d_%d_%d.jpg' % (self.kit_id, self.ticket, self.number)


class Answer(models.Model):
    """
    Модель варианта ответа на вопрос в билете.
    """
    id = models.IntegerField(primary_key=True)
    question = models.ForeignKey(
        Question, on_delete=models.CASCADE, verbose_name='вопрос')
    text = models.CharField('текст ответа', max_length=512)
    is_correct = models.BooleanField('правильный')

    class Meta:
        ordering = ('question', 'text')
        verbose_name = 'ответ'
        verbose_name_plural = 'ответы'

    def __str__(self):
        return self.text
