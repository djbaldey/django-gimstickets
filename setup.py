#
# Copyright (c) 2021, Grigoriy Kramarenko <root@rosix.ru>
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
"""
Данный установочный файл не является полноценным установщиком и не решает всех
зависимостей, а предназначен лишь для создания ссылки в виртуальном окружении
на локальный git-репозиторий, например так:

    pip install -e ~/src/django-gimstickets/

"""
from setuptools import setup, find_packages
import gimstickets

setup(
    name='GimsTickets',
    version=gimstickets.__version__,
    description='The Django-application for tickets from GIMS.',
    author='Grigoriy Kramarenko',
    author_email='root@rosix.ru',
    url='https://gitlab.com/djbaldey/django-gimstickets',
    platforms='any',
    zip_safe=False,
    packages=find_packages(),
    include_package_data=True,
    install_requires=[],
)
